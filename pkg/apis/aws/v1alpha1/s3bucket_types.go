package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// S3BucketSpec defines the desired state of S3Bucket
type S3BucketSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html

	// which region bucket is deployed to
	// +kubebuilder:validation:Enum=us-east-1;us-east-2;us-west-1;us-west-2
	Region string `json:"region"`

	// defines the bucket access control list
	// +kubebuilder:validation:Enum=private;public-read;public-read-write;aws-exec-read;authenticated-read;log-delivery-write
	ACL string `json:"acl,omitempty"`

	// determines whether older versions of bucket objects continue to be stored
	Versioning bool `json:"versioning,omitempty"`

	// determines whether objects are encrypted by default
	Encryption bool `json:"encryption,omitempty"`

	//permissions auto generated iam policy should grant pods
	// +kubebuilder:validation:Enum=read;write
	IAMPermissions string `json:"iamPermissions"`
}

// S3BucketStatus defines the observed state of S3Bucket
type S3BucketStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html

	// determines whether bucket has been created
	Created bool   `json:"created,omitempty"`
	Error   string `json:"error,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// S3Bucket is the Schema for the s3buckets API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=s3buckets,scope=Cluster
// +kubebuilder:resource:path=s3buckets,shortName=s3;bucket
// +kubebuilder:printcolumn:name="Created",type="boolean",JSONPath=".status.created",description="Whether the bucket has been created"
// +kubebuilder:printcolumn:name="Error",type="string",JSONPath=".status.error",description="error if bucket couldn't be created"
type S3Bucket struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   S3BucketSpec   `json:"spec,omitempty"`
	Status S3BucketStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// S3BucketList contains a list of S3Bucket
type S3BucketList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []S3Bucket `json:"items"`
}

func init() {
	SchemeBuilder.Register(&S3Bucket{}, &S3BucketList{})
}
