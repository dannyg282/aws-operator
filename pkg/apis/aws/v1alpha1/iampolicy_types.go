package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// IAMPolicySpec defines the desired state of IAMPolicy
type IAMPolicySpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html
	// Determines what AWS resource the IAM policy created is for
	// +kubebuilder:validation:Enum=s3;
	Type string `json:"type"`

	// Determines what permissions policy will allow
	// +kubebuilder:validation:Enum=read;write
	Permissions string `json:"permissions"`

	// Determines the name of the resource
	ResourceName string `json:"resourceName"`
}

// IAMPolicyStatus defines the observed state of IAMPolicy
type IAMPolicyStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book-v1.book.kubebuilder.io/beyond_basics/generating_crd.html

	// determines whether bucket has been created
	Created bool   `json:"created,omitempty"`
	Error   string `json:"error,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// IAMPolicy is the Schema for the iampolicies API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=iampolicies,scope=Cluster
// +kubebuilder:printcolumn:name="Type",type="string",JSONPath=".spec.type",description="error if resource couldn't be created"
// +kubebuilder:printcolumn:name="Created",type="boolean",JSONPath=".status.created",description="Whether the resource has been created"
// +kubebuilder:printcolumn:name="Error",type="string",JSONPath=".status.error",description="error if resource couldn't be created"
type IAMPolicy struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   IAMPolicySpec   `json:"spec,omitempty"`
	Status IAMPolicyStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// IAMPolicyList contains a list of IAMPolicy
type IAMPolicyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []IAMPolicy `json:"items"`
}

func init() {
	SchemeBuilder.Register(&IAMPolicy{}, &IAMPolicyList{})
}
