package controller

import (
	"gitlab.com/dannyg282/aws-operator/pkg/controller/s3bucket"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, s3bucket.Add)
}
