package s3bucket

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/go-logr/logr"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	awsv1alpha1 "gitlab.com/dannyg282/aws-operator/pkg/apis/aws/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const S3BucketFinalizer = "finalizer.s3.aws.test"

var log = logf.Log.WithName("controller_s3bucket")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new S3Bucket Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileS3Bucket{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("s3bucket-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource S3Bucket
	err = c.Watch(&source.Kind{Type: &awsv1alpha1.S3Bucket{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &awsv1alpha1.IAMPolicy{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &awsv1alpha1.S3Bucket{},
	})

	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileS3Bucket implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileS3Bucket{}

// ReconcileS3Bucket reconciles a S3Bucket object
type ReconcileS3Bucket struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a S3Bucket object and makes changes based on the state read
// and what is in the S3Bucket.Spec
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileS3Bucket) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Name", request.Name)
	reqLogger.Info("Reconciling S3Bucket")

	// Fetch the S3Bucket bucket
	bucket := &awsv1alpha1.S3Bucket{}
	err := r.client.Get(context.TODO(), request.NamespacedName, bucket)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	isBucketMarkedForDeletion := bucket.GetDeletionTimestamp() != nil
	if isBucketMarkedForDeletion {
		if contains(bucket.GetFinalizers(), S3BucketFinalizer) {
			bucket, err = DeleteS3Bucket(bucket)
			if err != nil {
				r.client.Status().Update(context.TODO(), bucket)
				return reconcile.Result{}, err
			}
			bucket.SetFinalizers(removeFinalizers(bucket.GetFinalizers(), S3BucketFinalizer))
			err := r.client.Update(context.TODO(), bucket)
			if err != nil {
				return reconcile.Result{}, err
			}
		}
		return reconcile.Result{}, nil
	}
	// Add finalizer for this CR
	if !contains(bucket.GetFinalizers(), S3BucketFinalizer) {
		if err := r.addFinalizer(reqLogger, bucket); err != nil {
			return reconcile.Result{}, err
		}
	}

	if bucket.Status.Created != true {
		reqLogger.Info("Creating a new S3 Bucket", "Bucket Name", bucket.Name, "Bucket Region", bucket.Spec.Region)
		bucket, err := newS3BucketForCR(bucket)
		if err != nil {
			err2 := r.client.Status().Update(context.TODO(), bucket)
			if err2 != nil {
				return reconcile.Result{}, err2
			}
			return reconcile.Result{}, err
		} else {
			err = r.client.Status().Update(context.TODO(), bucket)
			if err != nil {
				return reconcile.Result{}, err
			} else {
				return reconcile.Result{}, nil
			}
		}
	} else {
		reqLogger.Info("Bucket already exists, moving on", "Bucket Name", bucket.Name, "Bucket Region", bucket.Spec.Region)
	}

	bucket, err = configureS3Bucket(bucket)
	if err != nil {
		r.client.Status().Update(context.TODO(), bucket)
		return reconcile.Result{}, fmt.Errorf("unable to configure bucket %s : %w", bucket.Name, err)
	}
	log.Info("Bucket configured", "bucket", bucket.Name)

	iam := createIamPolicy(bucket)
	if err := controllerutil.SetControllerReference(bucket, iam, r.scheme); err != nil {
		r.client.Status().Update(context.TODO(), bucket)
		return reconcile.Result{}, err
	}
	found := &awsv1alpha1.IAMPolicy{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: iam.Name}, found)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new IAM Policy", "Bucket", bucket.Name, "Permissions", bucket.Spec.IAMPermissions)
		err = r.client.Create(context.TODO(), iam)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	} else {
		reqLogger.Info("Updating existing IAM Policy", "Bucket", bucket.Name, "Permissions", bucket.Spec.IAMPermissions)
		err = r.client.Update(context.TODO(), iam)
	}

	return reconcile.Result{}, nil
}

// newS3BucketForCR creates an s3 bucket in AWS
func newS3BucketForCR(cr *awsv1alpha1.S3Bucket) (*awsv1alpha1.S3Bucket, error) {

	svc := s3.New(session.New())
	input := &s3.CreateBucketInput{}
	if cr.Spec.Region == "us-east-1" {
		input = &s3.CreateBucketInput{
			ACL:    aws.String(cr.Spec.ACL),
			Bucket: aws.String(cr.Name),
		}
	} else {
		input = &s3.CreateBucketInput{
			ACL:    aws.String(cr.Spec.ACL),
			Bucket: aws.String(cr.Name),
			CreateBucketConfiguration: &s3.CreateBucketConfiguration{
				LocationConstraint: aws.String(cr.Spec.Region),
			},
		}
	}

	_, err := svc.CreateBucket(input)
	if err != nil {
		cr.Status.Error = err.Error()
		return cr, fmt.Errorf("unable to create bucket %s : %w", cr.Name, err)
	}
	err = svc.WaitUntilBucketExists(&s3.HeadBucketInput{
		Bucket: aws.String(cr.Name),
	})
	if err != nil {
		cr.Status.Error = err.Error()
		return cr, fmt.Errorf("error waiting for bucket %s to be created: %w", cr.Name, err)
	}
	cr.Status.Created = true
	log.Info("Bucket created", "bucket", cr.Name)

	cr.Status.Error = ""
	return cr, nil
}

func configureS3Bucket(cr *awsv1alpha1.S3Bucket) (*awsv1alpha1.S3Bucket, error) {
	log.Info("configuring bucket", "bucket", cr.Name)
	svc := s3.New(session.New())
	if cr.Spec.Encryption {
		defEnc := &s3.ServerSideEncryptionByDefault{
			SSEAlgorithm: aws.String(s3.ServerSideEncryptionAes256),
		}
		rule := &s3.ServerSideEncryptionRule{
			ApplyServerSideEncryptionByDefault: defEnc,
		}
		rules := []*s3.ServerSideEncryptionRule{rule}
		serverConfig := &s3.ServerSideEncryptionConfiguration{
			Rules: rules,
		}

		input := &s3.PutBucketEncryptionInput{
			Bucket:                            aws.String(cr.Name),
			ServerSideEncryptionConfiguration: serverConfig,
		}
		_, err := svc.PutBucketEncryption(input)
		if err != nil {
			cr.Status.Error = err.Error()
			return cr, fmt.Errorf("unable to encrypt bucket %s: %w", cr.Name, err)
		}
	}
	if cr.Spec.Versioning {
		defVer := &s3.VersioningConfiguration{
			Status: aws.String(s3.BucketVersioningStatusEnabled),
		}
		input := &s3.PutBucketVersioningInput{
			Bucket:                  aws.String(cr.Name),
			VersioningConfiguration: defVer,
		}
		_, err := svc.PutBucketVersioning(input)
		if err != nil {
			cr.Status.Error = err.Error()
			return cr, fmt.Errorf("unable to enable versioning on bucket %s: %w", cr.Name, err)
		}
	}

	return cr, nil
}

func DeleteS3Bucket(cr *awsv1alpha1.S3Bucket) (*awsv1alpha1.S3Bucket, error) {
	svc := s3.New(session.New())
	input := &s3.DeleteBucketInput{
		Bucket: aws.String(cr.Name),
	}
	log.Info("Deleting bucket...", "Bucket", cr.Name)
	_, err := svc.DeleteBucket(input)
	if err != nil {
		aerr := err.(awserr.Error)
		if aerr.Code() == s3.ErrCodeNoSuchBucket {
			log.Info("...Bucket already deleted!", "Bucket", cr.Name)
			return cr, nil
		}
		cr.Status.Error = err.Error()
		return cr, err
	}
	log.Info("...Waiting for bucket to be deleted...", "Bucket", cr.Name)
	err = svc.WaitUntilBucketNotExists(&s3.HeadBucketInput{
		Bucket: aws.String(cr.Name),
	})
	if err != nil {
		cr.Status.Error = err.Error()
		return cr, fmt.Errorf("error waiting for bucket to be deleted, %w", err)
	}
	log.Info("...Bucket successfully deleted!", "Bucket", cr.Name)
	return cr, nil
}

func createIamPolicy(cr *awsv1alpha1.S3Bucket) *awsv1alpha1.IAMPolicy {
	iam := &awsv1alpha1.IAMPolicy{
		ObjectMeta: v1.ObjectMeta{
			Name: cr.Name,
		},
		Spec: awsv1alpha1.IAMPolicySpec{
			Type:         "s3",
			Permissions:  cr.Spec.IAMPermissions,
			ResourceName: cr.Name,
		},
	}

	return iam
	//
	//err := r.client.Get(context.TODO(), request.NamespacedName, iam)
	//if err != nil {
	//	if errors.IsNotFound(err) {
	//		log.Info("Creating IAMPolicy in K8s for Bucket", "Bucket", cr.Name)
	//		err = r.client.Create(context.TODO(), iam)
	//		if err != nil {
	//			cr.Status.Error = err.Error()
	//			return cr, fmt.Errorf("unable to create iam policy:%w", err)
	//		}
	//	} else {
	//		cr.Status.Error = err.Error()
	//		return cr, fmt.Errorf("unable to query for iam policy:%w", err)
	//	}
	//} else {
	//	log.Info("Updating IAMPolicy in K8s for Bucket", "Bucket", cr.Name)
	//	iam.Spec.Permissions = cr.Spec.IAMPermissions
	//	err = r.client.Update(context.TODO(), iam)
	//	if err != nil {
	//		cr.Status.Error = err.Error()
	//		return cr, fmt.Errorf("unable to update iam policy:%w", err)
	//	}
	//}
	//
	//log.Info("IAMPolicy for Bucket applied in k8s", "Bucket", cr.Name)
	//
	//return cr, nil
}

func (r *ReconcileS3Bucket) addFinalizer(reqLogger logr.Logger, m *awsv1alpha1.S3Bucket) error {
	reqLogger.Info("Adding Finalizer for the Bucket")
	m.SetFinalizers(append(m.GetFinalizers(), S3BucketFinalizer))

	// Update CR
	err := r.client.Update(context.TODO(), m)
	if err != nil {
		reqLogger.Error(err, "Failed to update Memcached with finalizer")
		return err
	}
	return nil
}

func contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

func removeFinalizers(list []string, s string) []string {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}
